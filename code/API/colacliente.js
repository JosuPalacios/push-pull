const { connect, StringCodec, Subscription,
	AckPolicy, WorkQueuePolicy, consumerOpts, createInbox }
	= require("nats");
const assert = require("assert");
const codificador = StringCodec();
const { v4: uuidv4 } = require('uuid');

const DEBUG = true
function debug(msg) {
	debug.DEBUG = DEBUG

	if (debug.DEBUG) {
		console.log(msg)
	}
} // debug (msg)
function esperar(milis) {
	return new Promise(function (aceptar) {
		setTimeout(function () {
			aceptar()
		}, milis)
	})
} // esperar ()

class ColaCliente {
	// -----------------------------------------------------------------
	constructor(urlNats, { nombreCliente = null }) {
		// Inicializar
		debug("ColaCliente.constructor()");
		this.urlNats = urlNats
		this.nombreCliente = nombreCliente

		//  Crear un stream con dos subjects
		this.temaTrabajos = "trabajos"
		this.temaRespuestas = "respuestas"
		this.datosStream = {
			name: "colaDeTrabajos",
			subjects: [this.temaTrabajos,
			this.temaRespuestas + ".*"],
			retention: WorkQueuePolicy
		}

		// Conectar con nats e inicializar cliente
		return new Promise(async (resolver, rechazar) => {
			try {
				await this.conectarConNatsYCrearStream()
				//await this.iniciarCliente()
				resolver(this)
			} catch (error) {
				rechazar(error)
			}
		})
	} //  constructor (urlNats, {nombreCliente})

	// Función para conectar con Nats
	async conectarConNatsYCrearStream() {
		debug("ColaCliente.conectarConNatsYCrearStream()");

		// Conectar con NATS
		this.conexion = await connect(
			{
				servers: this.urlNats,
				maxReconnectAttempts: -1
			}
		)

		// Obtener manager y cliente de jetsream
		this.jsManager = await this.conexion.jetstreamManager()
		this.jsClient = await this.conexion.jetstream()

		// Crear el stream en nats-jetstrem
		await this.jsManager.streams.add(this.datosStream)
	} // conectarConNatsYCrearStream ()

	// Función para cerrar la conexión
	async cerrar() {
		debug("ColaCliente.cerrar()");
		// Cancelar la subscripción
		if (this.suscripcionCliente) {
			try {
				this.suscripcionCliente.destroy()
			} catch (error) {
				debug("colaCliente.cerrar() - this.suscripcionCliente.destroy()")
			}
		}

		// Cerrar la conexion con nats
		try {
			await this.conexion.close()
			await this.conexion.closed()
			debug("desconexion ok")
		} catch (error) {
			debug(error)
			throw "colacliente.js cerrar() " + err
		}
	} //  cerrar ()

	async anadirTrabajo(trabajo, callback = null, timeout = 1000) {
		debug("ColaCliente.anadirTrabajo()")

		try {
			assert(this.jsClient)

			if (!trabajo.idTrabajo) {
				trabajo.idTrabajo = uuidv4();
			}

			debug(" añadiendo trabajo: " + trabajo.idTrabajo)

			// 1. Si hay que  esperar la respuesta a este trabajo
			//  Primero se isntalla la espera
			if (callback) {
				let suscripcion = await this.conexion.subscribe(
					this.temaRespuestas + "." + trabajo.idTrabajo,

					{
						max: 1,
						callback: function (err, resp) {
							if (err) {
								callback(err, null)
								return
							}
							let respuesta = JSON.parse(codificador.decode(resp.data))
							callback(null, respuesta)
						}
					}
				)

				// si vence, cancelará la suscripción
				setTimeout(async () => {
					try {
						await suscripcion.close()
						callback("TIMEOUT esperando  RESPUESTA", null)
					} catch (error) {
						callback("TIMEOUT esperando  RESPUESTA:" + error, null)
					}
				}, timeout)
			}

			// 2. Envío el trabajo al stream
			await this.jsClient.publish(
				this.temaTrabajos,
				codificador.encode(JSON.stringify(trabajo)),
				{ msgID: this.temaTrabajos + "." + trabajo.idTrabajo }
			)

			return trabajo.idTrabajo
		} catch (error) {
			if (callback) {
				callback(error, null)
			}
			return null
		}
	} // anadirTrabajo ()

	async esperarRespuestaATrabajo(idTrabajo) {
		debug("ColaCliente.esperarRespuestaATrabajo()")
		assert(this.jsClient)
		let suscriId = null;

		try {
			// Buscar el trabajo que acabamos de mandar por su id
			suscriId = await this.jsClient.pullSubscribe(
				this.temaRespuestas + "." + idTrabajo, {
				config: { durable_name: uuidv4() }
			}
			)
			suscriId.pull()
			let respuesta = null;
			for await (const m of suscriId) {
				respuesta = codificador.decode(m.data);
				debug("Llega respuesta: " + respuesta)
				await m.ack();
				break;
			}
			return respuesta;
		} catch (error) {
			debug(error)
		} finally {
			if (suscriId) suscriId.unsubscribe();
		}

	} // esperarRespuestaATrabajo ()

} //  ColaCliente

module.exports = { ColaCliente, esperar };

