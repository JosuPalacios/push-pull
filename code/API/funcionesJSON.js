'use strict'

const { v4: uuidv4 } = require('uuid');

function calcularId(){
    return uuidv4();
}

function combinarJSON(id, tipoTarea, objeto){
    var trabajo = new Object();
    trabajo.idTrabajo = id;
    trabajo.tarea  = tipoTarea;
    trabajo.objeto = objeto;
    return trabajo;
}

module.exports = { calcularId, combinarJSON }