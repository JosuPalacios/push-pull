'use strict'

const { ColaCliente, esperar } = require("./colacliente.js")
const assert = require("assert");
const { v4: uuidv4 } = require('uuid');
const fs = require('fs');
const configFile = process.env.CONFIG_FILE;

var NATS_URL = null;

var readConfig = () => {
  return new Promise((resolve, reject) => {
    fs.readFile(configFile, "utf8", (err, jsonString) => {
      if (err) {
        reject(err)
        return
      }
      try {
        var appConfig = JSON.parse(jsonString);
        console.log(appConfig);
        console.log(appConfig.urlnats);
        resolve(appConfig);
      } catch (err) {
        reject(err)
      }
    })
  })
}

async function configurarCliente(trabajo) {
  console.log("conexionColaTrabajo.js - configurarCliente()")
  await readConfig().then((config) => {
    NATS_URL = config.urlnats;
  });
  var cdtClient;
  var nombreCliente = null;
  try {
    nombreCliente = uuidv4();
    cdtClient = await new ColaCliente(NATS_URL, { nombreCliente: nombreCliente });
    assert(cdtClient);

    const id = await cdtClient.anadirTrabajo(trabajo);
    return await cdtClient.esperarRespuestaATrabajo(trabajo.idTrabajo);
  } catch (error) {
    console.log(error);
  } finally {
    if (cdtClient) await cdtClient.cerrar();
  }
} // configurarCliente (trabajo)


module.exports = { configurarCliente }