'use strict'

var app = require("./app");
var port = process.env.HTTP_SERVER_PORT_ENV;

app.listen(port, () => {
    console.log("El servidor esta corriendo en localhost:3000 ...");
});
