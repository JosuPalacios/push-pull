const mariadb = require('mariadb');

var pool = null;

const fs = require('fs');
const configFile = process.env.CONFIG_FILE;

var readConfig = () => {
	return new Promise((resolve, reject) => {
		fs.readFile(configFile, "utf8", (err, jsonString) => {
			if (err) {
				reject(err)
				return
			}
			try {
				var appConfig = JSON.parse(jsonString);
				console.log(appConfig);
				console.log("urldb: " + appConfig.urldb);
				console.log("urlnats: " + appConfig.urlnats);
				resolve(appConfig);
			} catch (err) {
				reject(err)
			}
		})
	})
}

async function conectar() {
	await readConfig().then((config) => {
		pool = mariadb.createPool({
			host: config.urldb,
			user: 'root',
			database: "CCPUSHPULL",
			password: 'jjaibuspaldor',
			connectionLimit: 5
		});
	});
	console.log("HOST: " + pool.host);
	let conexion = await pool.getConnection();
	return conexion;
}

const insertarObjeto = async function (objeto) {
	let conexion;
	try {
		conexion = await conectar();
		var sql = "INSERT INTO trabajos (id, descripcion) VALUES (" + objeto.id + ", \"" + objeto.descripcion + "\");";
		const res = await conexion.query(sql);
		return res;
	} catch (err) {
		throw err;
	} finally {
		if (conexion) conexion.release();
	}
}

const leerObjeto = async function (id) {
	let conexion;
	try {
		conexion = await conectar();
		var sql = "SELECT id, descripcion FROM trabajos WHERE id=" + id + ";"
		const rows = await conexion.query(sql);
		return rows;
	} catch (err) {
		throw err;
	} finally {
		if (conexion) conexion.release();
	}
}

module.exports = {
	insertarObjeto,
	leerObjeto
};
