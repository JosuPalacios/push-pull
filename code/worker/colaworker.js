const { connect, StringCodec, Subscription,
	AckPolicy, WorkQueuePolicy, consumerOpts, createInbox }
	= require("nats");
const assert = require("assert");
const { v4: uuidv4 } = require('uuid');
const dbimplementation = require('./dbimplementation.js')
const codificador = StringCodec();

const DEBUG = true
function debug(msg) {
	debug.DEBUG = DEBUG

	if (debug.DEBUG) {
		console.log(msg)
	}
} // debug (msg)

function esperar(milis) {
	return new Promise(function (aceptar) {
		setTimeout(function () {
			aceptar()
		}, milis)
	})
} // esperar ()

class ColaWorker {
	// -----------------------------------------------------------------
	constructor(urlNats) {
		// Inicializar
		debug("ColaWorker.constructor()")
		this.urlNats = urlNats

		//  Crear un stream con dos subjects
		this.temaTrabajos = "trabajos"
		this.temaRespuestas = "respuestas"
		this.datosStream = {
			name: "colaDeTrabajos",
			subjects: [this.temaTrabajos,
			this.temaRespuestas + ".*"],
			retention: WorkQueuePolicy
		}

		// Conectar con nats e inicializar worker
		return new Promise(async (resolver, rechazar) => {
			try {
				await this.conectarConNatsYCrearStream()
				await this.iniciarWorker()
				resolver(this)
			} catch (error) {
				rechazar(error)
			}
		})
	} //  constructor (urlNats, nombreCliente)

	// Función para conectar con Nats
	async conectarConNatsYCrearStream() {
		debug("ColaWorker.conectarConNatsYCrearStream()")

		// abro conexión con nats y
		// obtengo un manager y un client de jetstream
		this.conexion = await connect(
			{ servers: this.urlNats }
		)

		this.jsManager = await this.conexion.jetstreamManager()
		this.jsClient = await this.conexion.jetstream()

		// Crear el stream en nats-jetstrem
		await this.jsManager.streams.add(this.datosStream)
	} // conectarConNatsYCrearStream ()

	// Inicializar cliente
	async iniciarWorker() {
		debug("ColaWorker.iniciarWorker()")

		try {
			this.workerDurableName = "WORKER"

			// pull subscription : pediremos mensaje con pull
			this.suscripcionWorker = await this.jsClient.pullSubscribe(
				this.temaTrabajos,
				{ config: { durable_name: this.workerDurableName } }
			)

			assert(this.suscripcionWorker)
			debug("ColaWorker.iniciarWorker() FIN")
		} catch (error) {
			debug(error)
			throw "inciarWorker() " + error
		}
	} //  inicializarWorker ()

	// Función para cerrar la conexión
	async cerrar() {
		// Cancelar la subscripción
		if (this.suscripcionWorker) {
			try {
				this.suscripcionWorker.destroy()
			} catch (error) { }
		}

		// Cerrar la conexion con nats
		await this.conexion.close()
		const err = await this.conexion.closed()

		if (!err) {
			debug("desconexion ok")
		} else {
			throw err
		}
	} //  cerrar ()

	async hacerTrabajo(trabajo) {
		assert(trabajo);
		assert(trabajo.tarea);
		assert(trabajo.objeto);
		var respuesta = new Object();

		if (trabajo.tarea === "guardar") {
			console.log("colaworker.js insertando trabajo")
			try {
				await dbimplementation.insertarObjeto(trabajo.objeto);
				respuesta.mensaje = "Objeto insertado correctamente!";
			} catch (error) {
				console.log(error);
				respuesta.mensaje = "No se ha podido insertar el objeto.";
			}
		}
		if (trabajo.tarea === "pedir") {
			console.log("colaworker.js leyendo trabajo")
			try {
				respuesta.mensaje = await dbimplementation.leerObjeto(trabajo.objeto);
			} catch (error) {
				respuesta.mensaje = "No se ha podido leer el objeto.";
			}
		}
		if (!respuesta.mensaje) respuesta.mensaje = "Operación no reconocida";
		return respuesta;
	} // hacerTrabajo(trabajo)

	async pedirTrabajoDeFormaContinua() {
		debug("ColaWorker.pedirTrabajoDeFormaContinua()")
		let m = null;

		// Intentar leer el mensaje
		try {
			m = await this.jsClient.pull(this.datosStream.name,
				this.workerDurableName)
		} catch (error) { }

		try {
			if (m == null) await esperar(1000);
			if (m != null) {
				let trabajo = JSON.parse(codificador.decode(m.data))
				trabajo.mensajeOriginal = m;

				let respuesta = await this.hacerTrabajo(trabajo);
				await this.responderATrabajo(trabajo, respuesta);
			}
		} catch (error) {
			throw " ColaWorker.pedirTrabajoDeFormaContinua()  " + error
		} finally {
			this.pedirTrabajoDeFormaContinua();
		}
	} // pedirTrabajoDeFormaContinua()

	async responderATrabajo(trabajo, respuesta) {
		debug("ColaWorker.responderATrabajo()")
		assert(this.suscripcionWorker)
		assert(trabajo.mensajeOriginal)
		assert(trabajo.idTrabajo)

		// Han de coincidir. Lo copio por si no lo pusieron
		respuesta.idTrabajo = trabajo.idTrabajo

		try {
			debug(" respondiendo a trabajo: " + respuesta.idTrabajo)
			// 1. respondo a trabajo dejándolo en el stream
			// en el tema respuestas.*
			// con el tema respuestas.idTrabajo
			await this.jsClient.publish(
				this.temaRespuestas + "." + trabajo.idTrabajo,
				codificador.encode(JSON.stringify(respuesta.mensaje)),

				{ msgID: this.temaRespuestas + "." + respuesta.idTrabajo }
			)

			// 1.1 hago ack como que he resuelto el trabajo 
			await trabajo.mensajeOriginal.ack()
			debug(`trabajo ${respuesta.idTrabajo} RESPONDIDO !!!!`)

		} catch (error) {
			debug(error)
			throw "responderATrabajo(): " + error
		}

	} // responderATrabajo (trabajo, respuesta)

	async esperarTrabajos() {
		debug("*******************************************************")
		debug("\n\nColaWorker.esperarTrabajos()")
		debug("*******************************************************")

		assert(this.suscripcionWorker)

		const NOMBRE = uuidv4()
		try {
			let suscri = await this.jsClient.pullSubscribe(
				this.temaTrabajos
				, {
					config: {
						durable_name: NOMBRE
						, callback: function (error, resp) {
							debug(" ++++++++++++")
							debug(" ++++++++++++")
						}
					} // config
					, callback: function (error, resp) {
						debug(" +++ 2 +++++++++")
						debug(" ++++ ++++++++")
					} // callback
				}
			)

			debug(" espesaraTrabajos(): pulling ")
			debug(" esperarTrabajos(): esperando ")

			let m = await this.jsClient.pull("colaDeTrabajos", NOMBRE)

			debug("1 ------------------------------------------")
			debug(m)
			debug("2 ------------------------------------------")
			debug(m.subject)
			debug("3 ------------------------------------------")
			debug(codificador.decode(m.data))
			debug("3.1 ------------------------------------------")
			debug("4 ------------------------------------------")
			let trabajo = JSON.parse(codificador.decode(m.data))
			debug("5 ------------------------------------------")
			debug(trabajo)
			debug("6 ------------------------------------------")
			debug(" espesaraTrabajos(): MENSAJE !!!! ")
		} catch (error) {
			console.log(error)
		}
	} // esperarTrabajos ()

} //  ColaWorker

module.exports = { ColaWorker, esperar };
