const { ColaWorker, esperar } = require("./colaworker.js")
const assert = require("assert")
const fs = require('fs');
const configFile = process.env.CONFIG_FILE;

var NATS_URL = null;

async function main() {
  try {
    await readConfig().then((config) => {
      NATS_URL = config.urlnats;
    });
    // Crear un worker
    var cdtWorker = await new ColaWorker(NATS_URL)
    assert(cdtWorker)

    // Capturar ctrl-c para cerrar
    process.on('SIGINT', async function () {
      console.log(" sigint capturada ! ")
      await cdtWorker.cerrar()
      process.exit(0)
    })

    // Ponemos esperando a que llegue un trabajo al worker
    cdtWorker.pedirTrabajoDeFormaContinua();
  } catch (error) {
    console.log(error)
    await cdtWorker.cerrar()
  }
}

readConfig = () => {
  return new Promise((resolve, reject) => {
    fs.readFile(configFile, "utf8", (err, jsonString) => {
      if (err) {
        reject(err)
        return
      }
      try {
        var appConfig = JSON.parse(jsonString);
        console.log(appConfig);
        console.log(appConfig.urlnats);
        resolve(appConfig);
      } catch (err) {
        reject(err)
      }
    })
  })
}

main();

