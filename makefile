default: help

help:
	@echo "----------------------------------------------"
	@echo  "Init the topology with:"
	@echo  "	-> make start <-"
	@echo  "	This will start the NATS server,"
	@echo  "	the API and by default only one worker."
	@echo  
	@echo  "It is possible to add more workers with:"
	@echo  "	-> make runworker <-"
	@echo  "	This will start another worker."
	@echo  
	@echo  "NATS server can be easily stopped with:"
	@echo  "	-> make stopnats <-"
	@echo  "	If there is one, this command will stop the NATS server."
	@echo  
	@echo "----------------------------------------------"

start: 
	make stopnats || true
	gnome-terminal -- bash -c "make runnats; exec bash" &
	sleep 1
	gnome-terminal -- bash -c "make runAPI; exec bash" &
	sleep 1
	gnome-terminal -- bash -c "make runworker; exec bash" &

runAPI:
	node ./code/API/index.js

runworker:
	node ./code/worker/initworker.js

pullnats:
	docker pull nats
	docker images
	
runnats:
	docker run --name natssrv --rm  -p 4222:4222 nats -js

stopnats:
	docker stop natssrv
	docker ps -a

rmcont:
	docker rm `docker ps -aq` || true

clean: rmcont
	@echo
	docker ps -a
	@echo
	docker images

