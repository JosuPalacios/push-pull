'use strict'

var request = require('request');

var api_url = 'http://localhost:3000/guardarObjeto';

var data = {
    id: 3,
    descripcion: 'Cliente Guardar'
}

request.put({url: api_url, method: 'PUT', json: data}, (error, response, body)=>{
  if(error) console.log(error)
    
  // Printing status code
  console.log(response.statusCode);
    
  // Printing body
  console.log(body);
});
