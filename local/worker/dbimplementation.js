const mariadb = require('mariadb');
const pool = mariadb.createPool({
	host: 'localhost',
	user:'root', 
	database: "CCPUSHPULL",
	password: 'jjaibuspaldor',
	connectionLimit: 5
});

async function conectar() {
	let conexion = await pool.getConnection();
	return conexion;
}

const insertarObjeto = async function (objeto) {
	let conexion;
	try{
		conexion = await conectar();
		var sql = "INSERT INTO trabajos (id, descripcion) VALUES ("+objeto.id+", \""+objeto.descripcion+"\");";
		const res = await conexion.query(sql);
		return res;
	} catch (err) {
		throw err;
	} finally {
		if (conexion) conexion.release();
	}
}

const leerObjeto = async function (id) {
	let conexion;
	try {
		conexion = await conectar();
		var sql = "SELECT id, descripcion FROM trabajos WHERE id="+id+";"
		const rows = await conexion.query(sql);
		return rows;
	} catch (err) {
		throw err;
	} finally {
		if (conexion) conexion.release();
	}
}

module.exports = {
    insertarObjeto,
    leerObjeto
};
