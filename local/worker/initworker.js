const { ColaWorker, esperar } = require("./colaworker.js")
const assert = require("assert")

const NATS_URL = "localhost:4222"

async function main() {
    try {
            // Crear un worker
            var cdtWorker = await new ColaWorker(NATS_URL)
            assert(cdtWorker)

            // Capturar ctrl-c para cerrar
            process.on('SIGINT', async function () {
                    console.log(" sigint capturada ! ")
                    await cdtWorker.cerrar()
                    process.exit(0)
            })

            // Ponemos esperando a que llegue un trabajo al worker
            cdtWorker.pedirTrabajoDeFormaContinua();
    } catch(error){
            console.log(error)
            await cdtWorker.cerrar()
    }
}

main();

