'use strict'

const { ColaCliente, esperar } = require("./colacliente.js")
const assert = require("assert");
const { v4: uuidv4 } = require('uuid');

const NATS_URL = "localhost:4222"

async function configurarCliente(trabajo){
    console.log("conexionColaTrabajo.js - configurarCliente()")
    var cdtClient;
    var nombreCliente = null;
    try {
        nombreCliente = uuidv4();
        cdtClient = await new ColaCliente(NATS_URL, { nombreCliente: nombreCliente});
        assert(cdtClient);
        
        const id = await cdtClient.anadirTrabajo(trabajo);
        return await cdtClient.esperarRespuestaATrabajo(trabajo.idTrabajo);
    } catch(error){
        console.log(error);
    } finally {
        if (cdtClient) await cdtClient.cerrar();
    }
} // configurarCliente (trabajo)

module.exports = { configurarCliente }