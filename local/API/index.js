'use strict'

var app = require("./app");
var port = 3000;

app.listen(port, () => {
    console.log("El servidor esta corriendo en localhost:3000 ...");
});
