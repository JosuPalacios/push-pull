'use strict'

const { configurarCliente, cerrarConexion } = require("./conexionColaTrabajo.js");
const { calcularId, combinarJSON  } = require("./funcionesJSON.js");
const express = require('express');
const assert = require("assert")
const app = express();

app.use(express.json());

app.put('/guardarObjeto', async (req, res) => {
    var id = calcularId();
    var trabajo = combinarJSON(id, 'guardar', req.body);
    var respuesta = await configurarCliente(trabajo);
    res.send(respuesta);
});

app.get('/pedirObjeto', async (req, res) => {
console.log(req.headers)
console.log(req.params)
    var parametro = req.query.id;
    var id = calcularId();
    var trabajo = combinarJSON(id, 'pedir', parametro);
    var respuesta = await configurarCliente(trabajo);
    res.send(respuesta);
});

module.exports = app;
