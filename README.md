# Proyecto de laboratorio: Push-pull
El concepto “Push-Pull” nace de la necesidad en la que un consumidor y un productor establecen una relación de dependencia. Por un lado, cuando el consumidor solicita datos al productor se denomina “pull”, mientras que cuando el productor erige una comunicación y proporciona los datos disponibles al consumidor/es, se refiere al “push”.

Los ficheros fuente que describen la aplicación se encuentran bajo el directorio [code](code/). El directorio [manifests](manifests/) contiene los manifiestos necesarios para desplegar la aplicación en Kumori. La aplicación completa para poner en marcha en localhost está en [local](local/).
## Autores
+ [Javier Bustillo](https://gitlab.com/jabuslop)
+ [Ibai Dorronsoro](https://gitlab.com/idorlar)
+ [Josu Palacios](https://gitlab.com/JosuPalacios)
## Topología
![Topología proyecto push-pull](images/KPaas.png)
## Instrucciones soportadas
El endpoint expuesto se encuentra en <b>ccpushpulljavijosuibai.vera.kumori.cloud</b> y se accede mediante https. Los clientes que se comunican con el endpoint pueden mandar peticiones de tipo PUT y GET, dependiendo de lo que quieran hacer. A continuación, información más detallada de las operaciones que soporta la arquitectura:

<table>
<tr>
<td><b> Instrucción </b></td> <td><b> URL </b></td><td><b> Body </b></td><td><b> Params </b></td><td><b> Respuesta en éxito </b></td><td><b> Respuesta en fracaso </b></td>
</tr>
<tr>
<td><b> PUT </b></td>
<td>https://ccpushpulljavijosuibai.vera.kumori.cloud/guardarObjeto</td>
<td>

```json
{ 
     "id": int, 
     "descripcion": string 
 }
```

</td>
<td></td>
<td>"Objeto insertado correctamente!"</td>
<td>"No se ha podido insertar el objeto."</td>
</tr>
<tr>
<td><b> GET </b></td>
<td>https://ccpushpulljavijosuibai.vera.kumori.cloud/pedirObjeto</td>
<td></td>
<td>id=int</td>
<td>

```json
{ 
     "id": int, 
     "descripcion": string 
 }
```

</td>
<td>"No se ha podido leer el objeto." </td>
</tr>
</table>

## Prerrequisitos
+ Kumori instalado y configurado según indicado en el siguiente enlace: https://docs.kumori.systems/kpaas/0.2.6/model-tutorials/preparing-environment.html
## Puesta en marcha
1. Clonar el repositorio y entrar a la carpeta
```sh
git clone https://gitlab.com/JosuPalacios/push-pull.git
cd push-pull/
```
2. Fetch de las dependencias
```sh
kumorictl fetch-dependencies
```

3. El fichero [util.sh](util.sh) es un fichero ya preparado para ayudar al usuario a desplegar la infraestructura necesaria. El siguiente comando printea una breve descripción de su uso:
```sh
./util.sh help
```

El servicio se despliega con estos tres comandos:
```sh
./util.sh deploy-inbound
./util.sh deploy-pushpull
./util.sh link
```
## Tests de integración
La comprobación del correcto funcionamiento del servicio desplegado se ha automatizado mediante unos tests de integración mínimos. Estos tests se ejecutan de la siguiente manera:
```sh
./util.sh test
```
Se hace lo siguiente:
- [x] Inserta un objeto con id=1 en la base de datos.
- [x] Lee un objeto con la id=1 de la base de datos.
- [x] Intenta leer un objeto que no existe en la base de datos.
- [x] Intenta insertar un objeto con un JSON no válido.

Si los tests son correctos, debería devolver un output como este:
```console
ccpushpull@ubuntu:~$ ./util.sh test
Tests de integración mínimos para mostrar el correcto funcionamiento del servicio desplegado.

Inserta un objeto con id=1 en la base de datos.
Objeto insertado correctamente!

Lee un objeto con la id=1 de la base de datos.
[{"id":1,"descripcion":"Este es el primer trabajo."}]

Intenta leer un objeto que no existe en la base de datos.
[]

Intenta insertar un objeto con un JSON no válido.
"No se ha podido insertar el objeto."

```

## Eliminando topología
Para limpiar lo desplegado el fichero [util.sh](util.sh) vuelve a ser de ayuda. El siguiente comando se encarga de eliminar todos los elementos desplegados:
```sh
./util.sh undeploy-all
```
Hay que tener en cuenta que, si el deploy y el inbound tienen un link atado no pueden ser eliminados. Por lo que es posible que el comando mencionado tenga que ser ejecutado más de una vez.
