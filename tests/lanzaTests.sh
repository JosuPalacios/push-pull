#!/bin/bash
if [ $# -ne 1 ]
  then
    echo "Usage "$0" [SERVICEURL]"
    echo ""
    exit 1
   else
    SERVICEURL=$1
fi

echo "Inserta un objeto con id=1 en la base de datos."
curl -X PUT -H "Content-Type: application/json" -d '{"id":1,"descripcion":"Esta es la descripción del objeto"}' ${SERVICEURL}/guardarObjeto
echo ""
echo ""

echo "Lee un objeto con la id=1 de la base de datos."
curl -X GET ${SERVICEURL}/pedirObjeto?id=1
echo ""
echo ""

echo "Intenta leer un objeto que no existe en la base de datos."
curl -X GET ${SERVICEURL}/pedirObjeto?id=99
echo ""
echo ""

echo "Intenta insertar un objeto con un JSON no válido."
curl -X PUT ${SERVICEURL}/guardarObjeto -H 'Content-Type: applicaction/json' -d "$(cat insertarObjeto-mal.json)"
echo ""
echo ""