package pushpull_service

import (
  k "kumori.systems/kumori/kmv"
  a "kumori.systems/examples/pushpull_service/components/api"
  w "kumori.systems/examples/pushpull_service/components/worker"
  n "kumori.systems/examples/pushpull_service/components/nats"
  d "kumori.systems/examples/pushpull_service/components/db"
)

#Manifest: k.#ServiceManifest & {

  ref: {
    domain: "kumori.systems.examples"
    name: "pushpull_service"
    version: [0,0,1]
  }

  description: {

    //
    // Kumori Component roles and configuration
    //

    // Configuration (parameters and resources) to be provided to the Kumori
    // Service Application.
    config: {
      parameter: {
        language: string
      }
      resource: {}
    }

    // List of Kumori Components of the Kumori Service Application.
    role: {
      api: k.#Role
      api: artifact: a.#Manifest
      
      worker: k.#Role
      worker: artifact: w.#Manifest
      
      nats: k.#Role
      nats: artifact: n.#Manifest
      
      db: k.#Role
      db: artifact: d.#Manifest
    }

    // Configuration spread:
    // Using the configuration service parameters, spread it into each role
    // parameters
    role: {
      api: {
        cfg: {
          parameter: {}
          resource: {}
        }
      }
      worker: {
        cfg: {
          parameter: {}
          resource: {}
        }
      }
      nats: {
        cfg: {
          parameter: {}
          resource: {}
        }
      }
      db: {
        cfg: {
          parameter: {}
          resource: {}
        }
      }
    }

    //
    // Kumori Service topology: how roles are interconnected
    //

    // Connectivity of a service application: the set of channels it exposes.
    srv: {
      server: {
        inicioservicio: { protocol: "http", port: 80 }
      }
    }

    // Connectors, providing specific patterns of communication among channels.
    connector: {
        apiconnector: { kind: "lb" }
        natsapi: { kind: "full" }
        natsworker: { kind: "full" }
        dbworker: { kind: "full" }
    }

    // Links specify the topology graph.
    link: {
        // Outside -> FrontEnd (LB connector)
        self: inicioservicio: to: "apiconnector"
        apiconnector: to: api: "restapi"
        
        api: canalnats: to: "natsapi"
        natsapi: to: nats: "servidornats"
        
        worker: canalnats: to: "natsworker"
        natsworker: to: nats: "servidornats"
        
        worker: canaldb: to: "dbworker"
        dbworker: to: db: "servidordb"
    }
  }
}
