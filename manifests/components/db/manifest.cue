package db

import (
  k "kumori.systems/kumori/kmv"
  "strconv"
)

#Manifest : k.#ComponentManifest & {

  ref: {
    domain: "kumori.systems.examples"
    name: "db"
    version: [0,0,1]
  }

  description: {

    srv: {
      server: {
        servidordb: { protocol: "http", port: 3306 }
      }
    }

    config: {
      // Frontend role configuration parameters
      parameter: {
        appconfig: {
          urlnats: "0.canalnats:4222"
          urldb: "0.canaldb:3306"
        }
      }
      resource: {}
    }

    size: {
      $_memory: "100Mi"
      $_cpu: "100m"
      $_bandwidth: "10M"
    }

    code: {

      db: {
        name: "db"

        image: {
          hub: {
            name: "registry.gitlab.com"
            secret: ""
          }
          tag: "josupalacios/push-pull/mariadb:latest"
        }

        mapping: {
          // Filesystem mapping: map the configuration into the JSON file
          // expected by the component
          filesystem: [
            {
              path: "/config/config.json"
              data: config.parameter.appconfig
              format: "json"
            }
          ]
          env: {
            CONFIG_FILE: value: "/config/config.json"
            HTTP_SERVER_PORT_ENV: value: strconv.FormatUint(srv.server.servidordb.port, 10)
          }
        }
      }

    }
  }
}
