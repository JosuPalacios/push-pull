package api

import (
  k "kumori.systems/kumori/kmv"
  "strconv"
)

#Manifest : k.#ComponentManifest & {

  ref: {
    domain: "kumori.systems.examples"
    name: "api"
    version: [0,0,1]
  }

  description: {

    srv: {
      server: {
        restapi: { protocol: "http", port: 3000 }
      }
      client: {
        canalnats: { protocol: "http" }
      }
    }

    config: {
      // Frontend role configuration parameters
      parameter: {
        appconfig: {
          urlnats: "0.canalnats:4222"
        }
      }
      resource: {}
    }

    size: {
      $_memory: "100Mi"
      $_cpu: "100m"
      $_bandwidth: "10M"
    }

    code: {

      api: {
        name: "api"

        image: {
          hub: {
            name: "registry.gitlab.com"
            secret: ""
          }
          tag: "josupalacios/push-pull/api4"
        }

        mapping: {
          // Filesystem mapping: map the configuration into the JSON file
          // expected by the component
          filesystem: [
            {
              path: "/config/config.json"
              data: config.parameter.appconfig
              format: "json"
            }
          ]
          env: {
            CONFIG_FILE: value: "/config/config.json"
            HTTP_SERVER_PORT_ENV: value: strconv.FormatUint(srv.server.restapi.port, 10)
          }
        }
      }

    }
  }
}

