package nats

import (
  k "kumori.systems/kumori/kmv"
  "strconv"
)

#Manifest : k.#ComponentManifest & {

  ref: {
    domain: "kumori.systems.examples"
    name: "nats"
    version: [0,0,1]
  }

  description: {

    srv: {
      server: {
        servidornats: { protocol: "http", port: 4222 }
      }
    }

    config: {
      // Frontend role configuration parameters
      parameter: {
        appconfig: {
          urlnats1: "0.canalnats1:4222"
        }
      }
      resource: {}
    }

    size: {
      $_memory: "100Mi"
      $_cpu: "100m"
      $_bandwidth: "10M"
    }

    code: {

      nats: {
        name: "nats"

        image: {
          hub: {
            name: "registry.gitlab.com"
            secret: ""
          }
          tag: "josupalacios/push-pull/nats:latest"
        }

        mapping: {
          // Filesystem mapping: map the configuration into the JSON file
          // expected by the component
          filesystem: [
            {
              path: "/config/config.json"
              data: config.parameter.appconfig
              format: "json"
            }
          ]
          env: {
            CONFIG_FILE: value: "/config/config.json"
            HTTP_SERVER_PORT_ENV: value: strconv.FormatUint(srv.server.servidornats.port, 10)
          }
        }
      }
    }
  }
}
