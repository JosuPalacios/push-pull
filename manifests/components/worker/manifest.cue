package worker

import (
  k "kumori.systems/kumori/kmv"
  "strconv"
)

#Manifest : k.#ComponentManifest & {

  ref: {
    domain: "kumori.systems.examples"
    name: "worker"
    version: [0,0,1]
  }

  description: {

    srv: {
      client: {
        canalnats: { protocol: "http" }
        canaldb: { protocol: "http" }
      }
    }

    config: {
      // Frontend role configuration parameters
      parameter: {
        appconfig: {
          urlnats: "0.canalnats:4222"
          urldb: "0.canaldb"
        }
      }
      resource: {}
    }

    size: {
      $_memory: "100Mi"
      $_cpu: "100m"
      $_bandwidth: "10M"
    }

    code: {

      worker: {
        name: "worker"

        image: {
          hub: {
            name: "registry.gitlab.com"
            secret: ""
          }
          tag: "josupalacios/push-pull/worker12:latest"
        }

        mapping: {
          // Filesystem mapping: map the configuration into the JSON file
          // expected by the component
          filesystem: [
            {
              path: "/config/config.json"
              data: config.parameter.appconfig
              format: "json"
            }
          ]
          env: {
            CONFIG_FILE: value: "/config/config.json"
          }
        }
      }

    }
  }
}
