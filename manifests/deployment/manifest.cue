package pushpull_deployment

import (
  k "kumori.systems/kumori/kmv"
  s "kumori.systems/examples/pushpull_service/service:pushpull_service"
)

#Manifest: k.#DeploymentManifest & {

  ref: {
    domain: "kumori.systems.examples"
    name: "pushpull_cfg"
    version: [0,0,1]
  }

  description: {

    service: s.#Manifest

    configuration: {
      // Assign the values to the service configuration parameters
      parameter: {
        language: "en"
      }
      resource: {}
    }

    hsize: {
      api: {
        $_instances: 1
      }
      worker: {
        $_instances: 2
      }
      nats: {
        $_instances: 1
      }
      db: {
        $_instances: 1
      }
    }

  }
}

// Exposed to be used by kumorictl tool (mandatory)
deployment: (k.#DoDeploy & {_params:manifest: #Manifest}).deployment
