#!/bin/bash

INBOUNDNAME="javi.josuibai/gureinbound"
DEPLOYNAME="javi.josuibai/guredepccpushpull"
SERVICEURL="ccpushpulljavijosuibai.vera.kumori.cloud"
CLUSTERCERT="cluster.core/wildcard.vera.kumori.cloud"
TESTFOLDER="tests/"

case $1 in

'deploy-inbound')
  kumorictl register inbound $INBOUNDNAME \
    --domain $SERVICEURL \
    --cert $CLUSTERCERT
  ;;

'deploy-pushpull')
  kumorictl register deployment $DEPLOYNAME \
    --deployment ./manifests/deployment \
    --comment "CCPUSHPULL service, nuestro servicio"
  ;;

'link')
  kumorictl link $DEPLOYNAME:inicioservicio $INBOUNDNAME
  ;;

# Test the volumes service
'test')
  echo "Tests de integración mínimos para mostrar el correcto"
  echo "funcionamiento del servicio desplegado."
  echo ""
  (cd ${TESTFOLDER} && ./lanzaTests.sh "https://${SERVICEURL}")
  ;;

# Undeploy all (secrets, inbounds, deployments)
'undeploy-all')
  kumorictl unlink $DEPLOYNAME:inicioservicio $INBOUNDNAME
  kumorictl unregister deployment $DEPLOYNAME --force
  kumorictl unregister inbound $INBOUNDNAME
  ;;

'help')
  echo "USAGE: $0 [option]"
  echo "    deploy-inbound"
  echo "    deploy-pushpull"
  echo "    link"
  echo "    undeploy-all"

esac
